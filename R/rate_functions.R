#' Age-structured parasite multiplication rates (the number of released
#' merozoites that successfully invade uRBCS of age `a`) in the circulation.
#'
#' @param u_rbc The RBC count.
#' @param pmf The parasite multiplication factor.
#' @param beta The (age-dependent) merozoite preference for uninfected RBCs.
#'
#' @export
alpha_c <- function(u_rbc, pmf, beta) {
  denom <- sum(beta * u_rbc)
  if (denom > 0) {
    pmf * (beta * u_rbc) / denom
  } else {
    rep(0, length(beta))
  }
}


#' Age-structured parasite multiplication rates (the number of released
#' merozoites that successfully invade uRBCS of age `a`) in the spleen.
#'
#' @param u_rbc The RBC count.
#' @param pmf The parasite multiplication factor.
#' @param beta The (age-dependent) merozoite preference for uninfected RBCs.
#'
#' @export
alpha_r <- function(u_rbc, pmf, beta) {
  denom <- sum(beta * u_rbc)
  if (denom > 0) {
    pmf * (beta * u_rbc) / denom
  } else {
    rep(0, length(beta))
  }
}


#' Age-structured release rate of reticulocytes from the bone marrow into the
#' circulation.
#'
#' @param p The parameter values for the scenario.
#' @param age The reticulocyte age.
#' @param u_rbc The RBC count (must be a scalar).
#'
#' @export
rho <- function(p, age, u_rbc) {
  if (length(u_rbc) != 1) {
    stop("rho(): argument u_rbc has length ", length(u_rbc), " != 1")
  }

  t_release <- rho_release_age(p, u_rbc)

  g <- min(20, exp(p$kappa * (p$U_ss - u_rbc)))

  g * (p$rho0 * as.numeric(age < t_release)
    + 10 * as.numeric(age >= t_release)
  )
}


#' The minimum age at which reticulocytes are released from the bone marrow
#' into the circulation.
#'
#' @param p The parameter values for the scenario.
#' @param u_rbc The RBC count (must be a scalar).
#'
#' @export
rho_release_age <- function(p, u_rbc) {
  if (u_rbc > p$U_ss) {
    p$T_R
  } else {
    u_frac <- pmin(1, u_rbc / p$U_ss)
    scale <- p$T_R - p$T_R_min
    slope <- p$rho_slope
    inflection <- p$rho_inflection
    p$T_R_min + scale / (1 + exp(- slope * (u_frac - inflection)))
  }
}


#' Age-structured rate of RBC sequestration into the microvasculature.
#'
#' @param p The parameter values for the scenario.
#' @param age The RBC age.
#'
#' @export
zeta <- function(p, age) {
  if (p$species == "Pf") {
    denom <- (age^p$zeta_sl + p$zeta_a50^p$zeta_sl)
    pr_max <- 0.99
    -log(1 - pr_max) * age^p$zeta_sl / denom
  } else if (p$species == "Pv") {
    rep(0, p$T_irbc)
  } else {
    stop('zeta(): invalid species "', p$species, '"')
  }
}


#' Phagocytosis rate of uninfected RBCs in the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param macrophages The macrophage count.
#' @param age The RBC age.
#'
#' @export
lambda_u <- function(p, macrophages, age) {
  out <- rep(NA, length(age))
  out[age <= p$T_M] <- 0
  out[age > p$T_M] <- p$lambdaU.sel
  out * macrophages
}


#' Phagocytosis rate of infected RBCs in the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param macrophages The macrophage count.
#'
#' @export
lambda_i <- function(p, macrophages) {
  p$lambdaI.sel * macrophages
}


#' Rate of uninfected RBC removal from the circulation into the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param age The RBC age.
#'
#' @export
delta_u <- function(p, age) {
  k1 <- -log(p$deltaU_kmin / p$deltaU_A) / 23 / 7
  range <- p$deltaU_kmax - p$deltaU_kmin
  denom <- (age - 1)^p$deltaU_g + (p$deltaU_c50)^p$deltaU_g
  p$deltaU_A * exp(-k1 * (age - 1)) +
    p$nu + p$deltaU_kmin +
    range * (age - 1)^p$deltaU_g / denom
}


#' Increase in uninfected RBC removal into the spleen due to RBC congestion.
#'
#' @param p The parameter values for the scenario.
#' @param net_u_rbc The total number of uninfected RBCs in the circulation.
#' @param net_i_rbc The total number of infected RBCs in the circulation.
#'
#' @export
delta_u_fold <- function(p, net_u_rbc, net_i_rbc) {
  net_rbc <- net_u_rbc + net_i_rbc
  denom <- (net_i_rbc^p$gdUloss + (p$UIdelta50uRBC * net_rbc)^p$gdUloss)
  1 + p$knuURBC * net_i_rbc^p$gdUloss / denom
}


#' Rate of uninfected RBC reentry into the circulation from the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param age The RBC age.
#'
#' @export
delta_u_prime <- function(p, age) {
  sigma <- p$sigma_deltaUr * 24
  mu <- log(p$mu_deltaUr * 24)
  p$mag_deltaUr * stats::dlnorm(age, mean = mu, sd = sigma)
}


#' Rate of infected RBC removal from the circulation into the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param age The RBC age.
#'
#' @export
delta_i <- function(p, age) {
  if (p$nospleen) {
    # No spleen, so RBCs cannot be retained in the spleen.
    0 * age
  } else {
    denom <- (age - 1)^p$deltaIa_slope + p$deltaIa_c50^p$deltaIa_slope
    numer <- (age - 1)^p$deltaIa_slope
    p$delta_iR + (p$delta_iS - p$delta_iR) * numer / denom
  }
}


#' Increase in infected RBC removal into the spleen due to RBC congestion.
#'
#' @param p The parameter values for the scenario.
#' @param net_u_rbc The total number of uninfected RBCs in the circulation.
#' @param net_i_rbc The total number of infected RBCs in the circulation.
#'
#' @export
delta_i_fold <- function(p, net_u_rbc, net_i_rbc) {
  net_rbc <- net_u_rbc + net_i_rbc
  denom <- (net_i_rbc^p$gdUloss + (p$UIdelta50iRBC * net_rbc)^p$gdUloss)
  1 + p$knuIRBC * net_i_rbc^p$gdUloss / denom
}


#' Rate of infected RBC reentry into the circulation from the spleen.
#'
#' @param p The parameter values for the scenario.
#' @param age The infected RBC age(s).
#'
#' @export
delta_i_prime <- function(p, age) {
  # Note that we use the same slope and half-maximal age as `delta_i()`.
  denom <- (age - 1)^p$deltaIa_slope + p$deltaIa_c50^p$deltaIa_slope
  numer <- (age - 1)^p$deltaIa_slope
  kmax <- p$delta_iS_prime - p$delta_iR_prime
  p$delta_iR_prime + kmax * numer / denom
}


#' Rate of RBC production.
#'
#' @param p The parameter values for the scenario.
#' @param u_rbc The RBC count.
#'
#' @export
erythropoiesis <- function(p, u_rbc) {
  p$gamma * (1 + (p$MaxFold - 1) * 0.5 *
               (1 - tanh(p$slope_e * (u_rbc - p$Ul) / p$Ul)))
}


#' Age-dependent merozoite preference for uninfected RBCs.
#'
#' @param p The parameter values for the scenario.
#' @param age The RBC age.
#'
#' @export
beta <- function(p, age) {
  if (p$species == "Pf") {
    pw <- p$sl_beta
    a50 <- p$a50_beta * 24
    beta_by_age <- a50^pw / ((age - 1)^pw + a50^pw) * exp(-5e-4 * (age - 1))
  } else if (p$species == "Pv") {
    sl <- p$Pv_slope * 24
    beta_by_age <- (1 - 1 / sl * (age - 1)) * as.numeric(age <= p$T_M)
  } else {
    stop('beta(): invalid species "', p$species, '"')
  }

  beta_by_age / sum(beta_by_age)
}
