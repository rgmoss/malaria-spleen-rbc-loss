library(dplyr)
library(readr)
library(usethis)

splenectomised <- read_csv(
  file.path("data-raw", "splenectomised.csv"),
  col_types = cols_only(
    patient = col_integer(),
    infection = col_character(),
    `total peri iRBC` = col_double(),
    `total spleen iRBC` = col_double(),
    `Total peri uRBC` = col_double(),
    `Total spleen uRBC` = col_double()
  )
) |>
  rename(
    irbc_peri = `total peri iRBC`,
    irbc_spleen = `total spleen iRBC`,
    urbc_peri = `Total peri uRBC`,
    urbc_spleen = `Total spleen uRBC`
  )

usethis::use_data(splenectomised, overwrite = TRUE)
