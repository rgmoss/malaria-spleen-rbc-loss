paramSet = function(inpPars = data.frame()){
  # In this function, I  set the values of the parameters in an array and feed it into the simulator
  

  LHS.param.names=colnames(inpPars)
  n.LHS.params = length(LHS.param.names)
  LHS.matrix = vector("list", length(LHS.param.names)) 
  names(LHS.matrix) = LHS.param.names
  
  

    ########################################################################################################
    # Generate Initial values/distributions of the params
    ########################################################################################################
    
  ## parameters that we found thier impact during our sensitivity analysis 
  ## (i.e., the model parameters that their contributions led to more than 10% change (increase and/or decrease) in the tested quantity)
  ## sampled from a triangular distribution (lower = 0.5*baselineValue < theta = baselineValue < upper = 1.5*baselineValue)
  lower_factor=0.80
  upper_factor=1.20
  
  
LHS.matrix$"deltaU_c50"=c(theta = inpPars$deltaU_c50, lower = lower_factor*inpPars$deltaU_c50, upper = upper_factor*inpPars$deltaU_c50)
LHS.matrix$"deltaU_g"=c(theta = inpPars$deltaU_g, lower = lower_factor*inpPars$deltaU_g, upper = upper_factor*inpPars$deltaU_g)     
LHS.matrix$"knuURBC"=c(theta = inpPars$knuURBC, lower = lower_factor*inpPars$knuURBC, upper = upper_factor*inpPars$knuURBC)      

LHS.matrix$"PMF"=c(theta = inpPars$PMF, lower = lower_factor*inpPars$PMF, upper = upper_factor*inpPars$PMF)       
LHS.matrix$"zeta_a50"=c(theta = inpPars$zeta_a50, lower = lower_factor*inpPars$zeta_a50, upper = upper_factor*inpPars$zeta_a50)      
 
LHS.matrix$"offset"=c(theta = inpPars$offset, lower = lower_factor*inpPars$offset, upper = upper_factor*inpPars$offset)    
LHS.matrix$"lambdaI.sel"=c(theta = inpPars$lambdaI.sel, lower = lower_factor*inpPars$lambdaI.sel, upper = upper_factor*inpPars$lambdaI.sel)  
LHS.matrix$"M_t_1"=c(theta = inpPars$M_t_1, lower = lower_factor*inpPars$M_t_1, upper = upper_factor*inpPars$M_t_1)  
LHS.matrix$"kM"=c(theta = inpPars$kM, lower = lower_factor*inpPars$kM, upper = upper_factor*inpPars$kM)  
LHS.matrix$"mu_deltaUr"=c(theta = inpPars$mu_deltaUr, lower = lower_factor*inpPars$mu_deltaUr, upper = upper_factor*inpPars$mu_deltaUr)   
 
LHS.matrix$"knuIRBC"=c(theta = inpPars$knuIRBC, lower = lower_factor*inpPars$knuIRBC, upper = upper_factor*inpPars$knuIRBC)  
LHS.matrix$"zeta_sl"=c(theta = inpPars$zeta_sl, lower = lower_factor*inpPars$zeta_sl, upper = upper_factor*inpPars$zeta_sl)  
   # LHS.matrix$"I_t_1"=c(theta = inpPars$I_t_1, lower = lower_factor*inpPars$I_t_1, upper = upper_factor*inpPars$I_t_1)  
  
LHS.matrix$"UIdelta50iRBC"=c(theta = inpPars$UIdelta50iRBC, lower = lower_factor*inpPars$UIdelta50iRBC, upper = upper_factor*inpPars$UIdelta50iRBC)  
  
#LHS.matrix$"offset_factor"=c(theta = inpPars$offset_factor, lower = 0.6*inpPars$offset_factor, upper = 1.4*inpPars$offset_factor)  
  
  #LHS.matrix$"Pv_slope"=c(theta = inpPars$Pv_slope, lower = lower_factor*inpPars$Pv_slope, upper = upper_factor*inpPars$Pv_slope)  
  
#LHS.matrix$"deltaIa_c50"=c(theta = inpPars$deltaIa_c50, lower = lower_factor*inpPars$deltaIa_c50, upper = upper_factor*inpPars$deltaIa_c50)  

LHS.matrix$"rho0"=c(theta = inpPars$rho0, lower = lower_factor*inpPars$rho0, upper = upper_factor*inpPars$rho0)  
LHS.matrix$"sigma_deltaUr"=c(theta = inpPars$sigma_deltaUr, lower = lower_factor*inpPars$sigma_deltaUr, upper = upper_factor*inpPars$sigma_deltaUr)  
LHS.matrix$"deltaU_A"=c(theta = inpPars$deltaU_A, lower = lower_factor*inpPars$deltaU_A, upper = upper_factor*inpPars$deltaU_A)  
LHS.matrix$"deltaU_kmin"=c(theta = inpPars$deltaU_kmin, lower = lower_factor*inpPars$deltaU_kmin, upper = upper_factor*inpPars$deltaU_kmin)  
LHS.matrix$"T_R_min"=c(theta = inpPars$T_R_min, lower = lower_factor*inpPars$T_R_min, upper = upper_factor*inpPars$T_R_min)  

  
  
  
  ## model parameters that we set them with their baselineValues
  
  LHS.matrix$"consUl"=c(theta=inpPars$consUl)
  LHS.matrix$"MaxFold"=c(theta=inpPars$MaxFold)       
#  LHS.matrix$"rho0"=c(theta=inpPars$rho0)          
  LHS.matrix$"kappa"=c(theta=inpPars$kappa)        
 # LHS.matrix$"knuIRBC"=c(theta=inpPars$knuIRBC)      
 # LHS.matrix$"UIdelta50iRBC"=c(theta=inpPars$UIdelta50iRBC) 
  LHS.matrix$"UIdelta50uRBC"=c(theta=inpPars$UIdelta50uRBC)
 # LHS.matrix$"sigma_deltaUr"=c(theta=inpPars$sigma_deltaUr)
  LHS.matrix$"mag_deltaUr"=c(theta=inpPars$mag_deltaUr)  
  LHS.matrix$"offset_factor"=c(theta=inpPars$offset_factor) 
#  LHS.matrix$"zeta_sl"=c(theta=inpPars$zeta_sl)       
  LHS.matrix$"lambdaU.sel"=c(theta=inpPars$lambdaU.sel)   
  LHS.matrix$"w_rc"=c(theta=inpPars$w_rc)         
  LHS.matrix$"I_t_1"=c(theta=inpPars$I_t_1)        
  LHS.matrix$"sl_beta"=c(theta=inpPars$sl_beta)       
  LHS.matrix$"a50_beta"=c(theta=inpPars$a50_beta)     
  LHS.matrix$"Pv_slope"=c(theta=inpPars$Pv_slope)      
#  LHS.matrix$"deltaU_A"=c(theta=inpPars$deltaU_A)      
  LHS.matrix$"deltaU_kmax"=c(theta=inpPars$deltaU_kmax)   
#  LHS.matrix$"deltaU_kmin"=c(theta=inpPars$deltaU_kmin)   
 
 #  LHS.matrix$"zeta_a50"=c(theta=inpPars$zeta_a50)   
 #  LHS.matrix$"kM"=c(theta=inpPars$kM)   
   
   LHS.matrix$"slope_e"=c(theta=inpPars$slope_e)   
#   LHS.matrix$"T_R_min"=c(theta=inpPars$T_R_min)   
   LHS.matrix$"deltaIa_slope"=c(theta=inpPars$deltaIa_slope)
   LHS.matrix$"deltaIa_c50"=c(theta=inpPars$deltaIa_c50)   
   
   
   
   
   
   
   
   
   
   
  
  # print(inpPars)
  LHS.matrix
}