#!/bin/bash

NAME="diagram"
xelatex "${NAME}"
convert -density 300 -strip "${NAME}.pdf" "${NAME}.png"
rm "${NAME}".{aux,log,pdf}
