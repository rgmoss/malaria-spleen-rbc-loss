# Investigating the role of the spleen in red blood cell loss in malaria

[![status badge](https://robmoss.r-universe.dev/badges/spleenrbc)](https://rgmoss.pages.gitlab.unimelb.edu.au/malaria-spleen-rbc-loss/)

This repository contains a mathematical model of red blood cell (RBC) dynamics in a malaria infection.
This model includes the spleen as a separate model compartment that can retain large amounts of infected and uninfected RBCs.

See the [online documentation](https://rgmoss.pages.gitlab.unimelb.edu.au/malaria-spleen-rbc-loss/) for vignettes and reference materials.

## Installation

The model is implemented [in R](https://www.r-project.org/) and provided as an R package called `spleenrbc`.
The simplest way to install this package is:

```R
install.packages("spleenrbc", repos = "https://robmoss.r-universe.dev")
library(spleenrbc)
```
You will then be able to run the baseline model scenarios, and define and run your own model scenarios.
See the provided [package documentation and vignettes](https://rgmoss.pages.gitlab.unimelb.edu.au/malaria-spleen-rbc-loss/) for examples.

## Running simulations and plotting results

The simulation and plotting scripts are not included in the package itself.
In order to run them, you will need to clone this repository:

```sh
git clone https://gitlab.unimelb.edu.au/rgmoss/malaria-spleen-rbc-loss.git
cd malaria-spleen-rbc-loss
```

### Running simulations

There are scripts to run the following simulations:

- `run-baseline-scenarios.R`: using the baseline model parameters, run a single simulation for the following scenarios:

  - No infection;
  - Pf infection; and
  - Pv infection.

- `run-sensitivity-analysis.R`: run 1000 simulations for Pf infections and 1000 simulations for Pv infections.
  Both sets of simulations use Latin hypercube sampling (LHS) and vary 18 of the baseline parameters by +/- 20%.
  Note that this may take **around 2 hours** to complete.

### Plotting results

Each simulation script saves the results to a separate CSV file in the `outputs/` directory.

You can plot these results with the `plot-scenarios.R` script.
For example, run the following command to plot the baseline Pf results:

```sh
./plot-scenarios.R outputs/baseline-pf.csv
```

### Sensitivity analyses

Summaries of each sensitivity analysis are provided as `.rds` files in the `outputs/` directory.
Summary plots for each sensitivity analysis can be produced by running:

```sh
./plot-sensitivity-analysis.R
```

## Code structure

The model implementation is divided into multiple `R` files:

- Model parameters:

  - `parameters.R` provides functions that return baseline parameter values (`baseline_parameters`) and calculate values for dependent parameters (`define_derived_parameters`).

  - `homeostasis.R` provides a function that calculates the reticulocyte production rate and red blood cell populations at homeostasis, in the absence of a malaria infection (`retic_steady_state`).

- Model state:

  - `state.R` provides functions that create the initial model state (`initial_spleenrbc_state`), simulate the model forward one time-step (`step_spleenrbc_model`), and perform an entire model simulation (`run_spleenrbc_model`).

  - `rate_functions.R` provides functions that calculate the time-varying rates that govern the movement and destruction of red blood cells in the model.

- Simulation outputs:

  - `outputs.R` provides functions that record key outputs of a each simulation (`individual_results`), and calculate derived quantities of interest (`infection_flows_and_loss`, `calculate_irbc_ratio`, `credible_intervals`, `rbc_counts_to_haemoglobin`).

- Running model simulations:

  - `run.R` provides a function for running multiple model simulations (`run_spleenrbc`).

  - `sensitivity.R` provides functions that define sampling distributions for the sensitivity analysis (`lhs_distributions`) and draw parameter samples for the sensitivity analysis (`sample_parameters`).
